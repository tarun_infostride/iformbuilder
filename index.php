<?php
include 'config.php';
include 'acessAPI.php';

$APIObj = new Token();
$a = $APIObj->getData();
print_r($a);die;
// On Submit of the form..
if(isset($_POST['submit'])){
	if (isset($_POST['firstName'])){
		$FirstName = $_POST['firstName'];
	}
	if (isset($_POST['lastName'])){
		$LastName = $_POST['lastName'];
	}
	$APIObj->AddFormDetails($FirstName,$LastName);
	$_POST['success']="Submit";
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>IFormBuilder</title>
		
		<!-- CDN for Jquery -->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		
		<!-- Bootstrap CSS and Minified JS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
		
		
		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
		<script src="jquery.barrating.min.js"></script>
		
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="customStyle.css" />
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<p>This is a sample Form Page.</p>
					
					<div class="col-md-5 IformBuilder_form">
					<p id="errorAdd" class="row"></p>
						<?php if(!isset($_POST['success'])){?>
						<form method="post" action="index.php">
							<div class="form-group row">
								<label for="formGroupFirstNameInput">First Name<span>*</span></label>
								<input type="text" name="firstName" class="form-control" id="formGroupFirstNameInput" placeholder="First Name">
							</div>
							<div class="form-group row">
								<label for="formGroupLastNameInput">Last name<span>*</span></label>
								<input type="text" name="lastName" class="form-control" id="formGroupLastNameInput" placeholder="Last Name">
							</div>
							
							<button type="submit" id="submit" name="submit" class="btn btn-primary" onclick="return  submitForm()">Submit</button>
							
						</form><?php }else{ ?>
						<h2 class="success">Sucessfully submitted the data <a href="index.php">Back</a></h2>
						<?php } ?>
					</div>
				</div>
			
			</div>				
		</div>
	
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
	  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
	  
	  $("#tel-input").mask("(000) 000-0000"); 
});

function submitForm(){
	var error = "false";
	 if($("#formGroupFirstNameInput").val() == null || $("#formGroupFirstNameInput").val().trim().length == 0){
		 $("#formGroupFirstNameInput").addClass("error");
		 error = "true";
	 }else{
		 $("#formGroupFirstNameInput").removeClass("error");
	 }
	 if($("#formGroupLastNameInput").val() == null || $("#formGroupLastNameInput").val().trim().length == 0){
		 $("#formGroupLastNameInput").addClass("error");
		 error = "true";
	 }else{
		 $("#formGroupLastNameInput").removeClass("error");
	 }
	 
	 if(error == "true"){
		 document.getElementById("errorAdd").innerHTML = "Complete the details!!";
		 document.getElementById('errorAdd').scrollIntoView();
		 return false;
	 }else{
		 document.getElementById("errorAdd").innerHTML = "";
		 return true;
	 }
};	
	</script>
	
	</body>
</html> 