<?php
include 'config.php';
Class Token{
	  // private constructor function
	  // to prevent external instantiation
	public function Token() {
	}
	
	function base64url_encode($data) { 
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
	} 
	function base64url_decode($data) { 
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
	} 
	//to generate a token and get an access token
	private function generateToken(){
		$post_Header = base64_encode(json_encode(array("alg" => "HS256","typ" => "JWT")));
		$header = [
                  'typ' => 'JWT',
                  'alg' => 'HS256'
        ];
		// Returns the JSON representation of the header
		$header = json_encode($header);
		//encodes the $header with base64.	
		$header = base64_encode($header);
		
		$CLIENT_KEY = "29455e1e2e895dc427110a9a6e0c0f4bae609539";
		$AUD_VALUE = "https://app.iformbuilder.com/exzact/api/oauth/token";
		$CLIENT_SECRET = "554cc6c8dfbdd341c726c72dbbf4ac64e0739560";
		$nowtime = time();
		$exptime = $nowtime + 599;
		
		$payload = "{
			\"iss\": \"$CLIENT_KEY\",
		   \"aud\": \"$AUD_VALUE\",
		  \"exp\": $exptime,
		  \"iat\": $nowtime}";	
		$payload = $this->base64url_encode($payload);
		
		
		$signature = $this->base64url_encode(hash_hmac('sha256',"$header.$payload",$CLIENT_SECRET, true));
		$assertionValue = "$header.$payload.$signature";
		
		$grant_type = "urn:ietf:params:oauth:grant-type:jwt-bearer";
		$grant_type = urlencode($grant_type);
		$postField= "grant_type=".$grant_type."&assertion=".$assertionValue;	
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_URL, AUD_VALUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS,"$postField");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/x-www-form-urlencoded",
		  "cache-control: no-cache"
		));
		$response = curl_exec($ch);
		$headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		curl_close($ch);
		
		$tokenArray = json_decode($response,true);
		
		return $token = $tokenArray['access_token'];

	}
	
	public function AddFormDetails($FirstName,$LastName){
		//Initiate curl and send the data through API by using generateToken function
			$FirstName =	$this->clean($FirstName);
			$LastName = $this->clean($LastName);
			//generating a Token for access
			$JWToken = $this->generateToken();
			
			$jsonPostFields = "[{
			  \"fields\":[
				{
				  \"element_name\": \"first_name\",
				  \"value\": \"$FirstName\"
				},
				{
				  \"element_name\": \"last_name\",
				  \"value\": \"$LastName\"
				}
			  ]
			}]";
			
			$ch1 = curl_init();

			curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch1, CURLOPT_URL, RECORDURL);
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch1, CURLOPT_HEADER, false);

			curl_setopt($ch1, CURLOPT_POST, TRUE);

			curl_setopt($ch1, CURLOPT_POSTFIELDS, $jsonPostFields);

			curl_setopt($ch1, CURLOPT_HTTPHEADER, array(
			  "Content-Type: application/json",
			  "cache-control: no-cache",
			  "Authorization: Bearer $JWToken"
			));

			$response = curl_exec($ch1);
			curl_close($ch1);
				
	}
	public function getData(){
		$JWToken = $this->generateToken();
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://app.iformbuilder.com/exzact/api/v60/profiles/504439/pages/3861844/records?fields[]=last_name&fields[]=first_name",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer $JWToken"
		),
		));

		$response = curl_exec($curl);
		$data = json_decode($response, true);

		return $data;
	}
	function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	function cleanSpace($string) {
		$string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
		return $string = trim(preg_replace('/\s\s+/', ' ', $string));
	}
	
}
?>